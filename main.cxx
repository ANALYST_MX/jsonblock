#include "main.hxx"

int main(int argc, char **argv)
{
  JsonValue json;
  json["int"] = 2;
  json["float"] = 3.14f;
  json["string"] = "hello there";
  json["person"]["name"] = "Anthony Bongers";
  json["person"]["age"] = 22;
  json["person"]["height"] = 4.8f;
  json["person"]["weight"] = 340;
  json["person"]["programmer"] = true;

  std::cout << json.toString() << std::endl;


  return EXIT_SUCCESS;
}
