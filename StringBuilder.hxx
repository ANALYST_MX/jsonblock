#pragma once

#include <string>
#include <deque>
#include <numeric>

template <typename chr>
class StringBuilder
{
  typedef std::basic_string<chr> string_t;
public:
  StringBuilder(const string_t &src)
  {
    if (!src.empty()) {
        m_Data.push_back(src);
      }
    m_totalSize = src.size();
  }

  StringBuilder()
    : m_totalSize(0)
  {
  }

  StringBuilder & append(const string_t &src)
  {
    m_Data.push_back(src);
    m_totalSize += src.size();
    return *this; // allow chaining.
  }

  StringBuilder & operator << (const string_t &src)
  {
    return append(src);
  }

  template<class inputIterator>
  StringBuilder & add(const inputIterator &first, const inputIterator &afterLast)
  {
    for (inputIterator f = first; f != afterLast; ++f)
      {
        append(*f);
      }
    return *this;
  }

  StringBuilder & appendLine(const string_t &src)
  {
    static chr lineFeed[] { 13, 10, 0 };
    m_Data.push_back(src + lineFeed);
    m_totalSize += 1 + src.size();
    return *this;
  }

  StringBuilder & appendLine()
  {
    static chr lineFeed[] { 13, 10, 0 };
    m_Data.push_back(lineFeed);
    ++m_totalSize;
    return *this;
  }

  string_t toString() const
  {
    string_t result;
    result.reserve(m_totalSize + 1);

    for (auto iter = m_Data.begin(); iter != m_Data.end(); ++iter)
      {
        result += *iter;
      }
    return result;
  }

  string_t join(const string_t &delim) const
  {
    if (delim.empty())
      {
        return toString();
      }
    string_t result;
    if (m_Data.empty())
      {
        return result;
      }
    size_type st = (delim.size() * (m_Data.size() - 1)) + m_totalSize + 1;
    result.reserve(st);
    struct adder
    {
      string_t m_Joiner;
      adder(const string_t &s)
        : m_Joiner(s)
      {
      }

      string_t operator()(string_t &l, const string_t &r)
      {
        l += m_Joiner;
        l += r;
        return l;
      }
    } adr(delim);
    auto iter = m_Data.begin();
    result += *iter;
    return std::accumulate(++iter, m_Data.end(), result, adr);
  }
private:
  typedef std::deque<string_t> container_t;
  typedef typename string_t::size_type size_type;
  container_t m_Data;
  size_type m_totalSize;
  StringBuilder(const StringBuilder &);
  StringBuilder & operator = (const StringBuilder &);
};
