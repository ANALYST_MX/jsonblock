#include "JsonValue.hxx"

JsonValue::JsonValue()
  : type(JOBJECT)
{
}

JsonValue & JsonValue::operator[](const std::string &key)
{
  return values[key];
}

void JsonValue::clear()
{
  values.clear();
}

std::string JsonValue::toString()
{
  std::map<const std::string, JsonValue>::iterator it;
  StringBuilder<char> buffer;

  buffer << "{";

  for (it = values.begin(); it != values.end(); ++it)
    {
      if(it != values.begin())
        {
          buffer << ",";
        }

      buffer << "\"" << it->first << "\":";

      switch (it->second.type) {
        case JSTRING:
          buffer << "\"" << it->second.stringValue() << "\"";
          break;
        case JINT:
          buffer << std::to_string(it->second.intValue());
          break;
        case JFLOAT:
          buffer << std::to_string(it->second.floatValue());
          break;
        case JBOOL:
          buffer << (it->second.boolValue() ? "true" : "false");
          break;
        case JOBJECT:
          buffer << it->second.toString();
          break;
        default:
          break;
        }
    }

  buffer << "}";
  return buffer.toString();
}

std::string JsonValue::stringValue() const
{
  return *value.stringValue;
}

int JsonValue::intValue() const
{
  return value.intValue;
}

float JsonValue::floatValue() const
{
  return value.floatValue;
}

bool JsonValue::boolValue() const
{
  return value.boolValue;
}

JsonValue & JsonValue::operator=(int value)
{
  this->type = JINT;
  this->value.intValue = value;
  return *this;
}

JsonValue & JsonValue::operator=(float value)
{
  this->type = JFLOAT;
  this->value.floatValue = value;
  return *this;
}

JsonValue & JsonValue::operator=(bool value)
{
  this->type = JBOOL;
  this->value.boolValue = value;
  return *this;
}

JsonValue & JsonValue::operator=(const char *value)
{
  this->type = JSTRING;
  this->value.stringValue = new std::string(value);
  return *this;
}
