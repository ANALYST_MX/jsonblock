#pragma once

#include <string>
#include <map>
#include <iostream>

#include "JsonType.hxx"
#include "StringBuilder.hxx"

class JsonValue
{
public:
  JsonValue();
  void clear();
  std::string toString();
  JsonValue & operator[](const std::string &);
  std::string stringValue() const;
  int intValue() const;
  float floatValue() const;
  bool boolValue() const;
  JsonValue & operator=(int);
  JsonValue & operator=(float);
  JsonValue & operator=(bool);
  JsonValue & operator=(const char *);

private:
  struct ValueType {
    union {
      std::string *stringValue;
      int intValue;
      float floatValue;
      bool boolValue;
    };
  } value;
  JsonType type;
  std::map<const std::string, JsonValue> values;
};
