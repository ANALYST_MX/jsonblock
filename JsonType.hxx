#pragma once

enum JsonType
{
  JINT,
  JFLOAT,
  JBOOL,
  JSTRING,
  JNONE,
  JOBJECT,
};
