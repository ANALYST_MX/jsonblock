QT += core
QT -= gui

TARGET = JsonBlock
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cxx \
    JsonValue.cxx

HEADERS += \
    main.hxx \
    JsonType.hxx \
    JsonValue.hxx \
    StringBuilder.hxx \
    JsonBlock.hxx

CONFIG += c++11
